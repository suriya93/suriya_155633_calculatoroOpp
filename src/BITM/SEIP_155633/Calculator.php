<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 1/18/2017
 * Time: 12:23 PM
 */

namespace App;


class Calculator
{


    private  $number1;
    private  $number2;
    private  $operation;
    private $result;


    public function setNumber1($number1)
    {
        $this->number1 = $number1;
    }

    public function getNumber1()
    {
        return $this->number1;
    }

    public function setNumber2($number2)
    {
        $this->number2 = $number2;
    }

    public function getNumber2()
    {
        return $this->number2;
    }

    public function setOperation($operation)
    {
        $this->operation = $operation;
    }

    public function getOperation()
    {
        return $this->operation;
    }
    public function setResult($result)
    {


        switch($this->operation){


            case "Addition":
                $this->result = $this->doAddition();
            break;

            case "Subtraction":
                $this->result = $this->doSubtruction();
                break;

            case "Multiplication":
                $this->result = $this->doMultipication();
                break;

            case "Division":
                $this->result = $this->doDivision();
                break;
        }
    }

    public function getResult()
    {


        $this->setResult();
        return $this->result;

    }
    public function  doAddition($number1, $number2){


        return $this->$number1+$number2;


    }

    public function  doSubtruction($number1, $number2){


        return $this->$number1-$number2;

    }

    public function  doMultipication($number1, $number2){


        return $this->$number1*$number2;


    }
    public function  doDivision($number1, $number2){


        return $this->$number1/$number2;


    }



}